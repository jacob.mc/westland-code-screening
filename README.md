# Coding Interview

## Scenario
Westland Real Estate Group owns several rental units in the Los Angeles area. It has been discovered that some of the maintenance work orders for our rental units are being stored within Excel files and that employees are manually sharing these files with each other via email.

Unfortunately, this isn't particularly efficient, and since we have access to the work order data directly from the source database, we can begin to automate this process.

## Overview
We need to process this CSV into a more useful form which we can send to our internal emailing system, which will notify each point of contact about their assigned work orders.

## Task Requirements
After speaking with the users that will be affected by the automation of this process, we have determined the series of steps that need to be taken to properly process the work orders.

1. Any `part` number that ends in **1** or **7** will need to be excluded from the data. These parts were included in a manufacturing defect recall and their work orders will need to be reviewed independently.

2. The work orders should be processed into a suitable **typed** form and returned from the `processWorkOrders` function. The exact type is up for you to decide, but it should be suitable for the next step in the pipeline, where it will be consumed by a bot which will automatically email each contact their respective work orders. 

After consulting StackOverflow for guidance, our team has generated the following calls for you to use.

1. `fetchWorkOrders(): WorkOrder[]` will give you an array of objects which contain all the necessary `WorkOrder` information. In a real-world scenario, you can envision this as an actual API call to another service of ours.

2. `getContact(department: Departments, part: number) : string` will return the contact that any given work order needs to be sent to, by category/department and part number. 


Finally, we have created the `process-wo.ts` file for you to use. The `processWorkOrders` function is what our theoretical service will call. We expect that your result will be returned from within this function. You may use any number of helper functions to assist in generating your results.

## Conclusion and Final details

While there are some hard requirements as listed above, please feel free to modify or optimize your code and output as you see fit, appropriate to the task.

We estimate this task to take approximately 30 minutes to complete (after setup). You may take as much time as you wish, but please try not to spend more time than that. We want to be as respectful to your time as possible.

Although the solution to this project is somewhat open ended, please be sure to do the following.

1. Clone (or fork) this repo.
2. Return the results of your processing inside the `processWorkOrders` function
3. Upon completion of the task, make a Merge Request through Gitlab.

## Setup



1. Clone the repo (or fork, if that's your style )

2. Navigate to the root directory

3. Run `npm install`

4. Have fun!
