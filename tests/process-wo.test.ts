import { areYouAwesome } from '../src/process-wo';

/**
 * Perhaps the world's most important test
 *
 * Validates the expected handling of `areYouAwesome`
 * against yourself
 */
test('Are you an awesome developer?', () => {
  const isAnAwesomeDev = areYouAwesome();
  expect(isAnAwesomeDev).toBe(true);
});
