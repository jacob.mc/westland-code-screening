import { processWorkOrders } from './process-wo';

/**
 * You can call your work here, we'll be calling this function in our test
 * of your work.
 */
processWorkOrders()
  .then((r) => console.log(r))
  .catch((r) => console.error(r));
